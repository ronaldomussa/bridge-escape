﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    CharacterController controller;
    public GameControlScript control;
    

    public AudioSource VidaColetadaSound;
    public AudioSource BolaPColididaSound;
    public AudioSource ContatoSound;
    public AudioSource PuloSound;

    public float speed = 7.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    private Vector3 moveDirection = Vector3.zero;


    #if UNITY_ANDROID
        Vector3 zeroAcc;  //zero reference input.acceleration
        Vector3 currentAcc;  //In-game input.acceleration
        float sensitivityH = 4; //alter this to change the sensitivity of the accelerometer
        float smooth = 0.5f; //determines how smooth the acceleration(horizontal movement, in our case) control is
        float GetAxisH = 0;  //variable used to hold the value equivalent to Input.GetAxis("Horizontal")
    #endif


    void Start()
    {
        
        controller = GetComponent<CharacterController>();

        #if UNITY_ANDROID
            zeroAcc = Input.acceleration;
            currentAcc = Vector3.zero;
        #endif
    }

    // Update is called once per frame
    void Update()
    {

        #if UNITY_ANDROID
            currentAcc = Vector3.Lerp(currentAcc, Input.acceleration - zeroAcc, Time.deltaTime / smooth);
            GetAxisH = Mathf.Clamp(currentAcc.x * sensitivityH, -1, 1);
            int fingerCount = 0;
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
                    fingerCount++;
            }
        #endif

        if (controller.isGrounded)
        {

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);  //get keyboard input to move in the horizontal direction

            #if UNITY_ANDROID
                moveDirection = new Vector3(GetAxisH, 0, 0);
            #endif

            moveDirection = transform.TransformDirection(moveDirection);  //apply this direction to the character
            moveDirection *= speed;            //increase the speed of the movement by the factor "speed" 


            #if UNITY_ANDROID
                if (fingerCount >= 1)
                {
                    PuloSound.Play();
                    moveDirection.y = jumpSpeed;
                }
            #endif

            if (Input.GetButton("Jump"))
            {   
                PuloSound.Play();
                moveDirection.y = jumpSpeed;
            }
            
        }

        moveDirection.y -= gravity * Time.deltaTime;       //Apply gravity  
        controller.Move(moveDirection * Time.deltaTime);      //Move the controller
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Vida(Clone)")
        {
            control.VidaColetada();
            VidaColetadaSound.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.name == "BolaP(Clone)")
        {
            control.BolaColidida();
            BolaPColididaSound.Play();
            ContatoSound.Play();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.name == "BolaG(Clone)")
        {
            control.BolaColidida();
            BolaPColididaSound.Play();
            ContatoSound.Play();
            Destroy(other.gameObject);
        }

        if (other.gameObject.name == "Fogo(Clone)")
        {
            control.FogoAtingido();            
        }
    }
}
