﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

    public GameObject BolaP;
    public GameObject BolaG;
    public GameObject Vida;
    public GameObject Fogo;

    float timeElapsed = 0;
    float spawnCycle = 0.5f;

    void Update()
    {
        timeElapsed += Time.deltaTime;

        if (timeElapsed > spawnCycle)
        {
            GameObject temp;

            int obj = Random.Range(0, 7);

            Vector3 inicialPos = new Vector3(0, 1.3f, 40);

            if(obj == 0 || obj == 3 )
            {
                temp = (GameObject)Instantiate(Vida);
                temp.transform.position = new Vector3(Random.Range(-3, 4), inicialPos.y, inicialPos.z);
            }

            if(obj == 1 || obj == 4 )
            {
                temp = (GameObject)Instantiate(BolaP);
                temp.transform.position = new Vector3(Random.Range(-3, 4), inicialPos.y, inicialPos.z);
            }

            if (obj == 2 || obj == 5 )
            {
                temp = (GameObject)Instantiate(BolaG);
                temp.transform.position = new Vector3(Random.Range(-3, 4), inicialPos.y, inicialPos.z);
            }

            if (obj == 6)
            {
                temp = (GameObject)Instantiate(Fogo);
                temp.transform.position = new Vector3(Random.Range(-3, 4), inicialPos.y, inicialPos.z);
            }

            timeElapsed -= spawnCycle;
            
        }
    }
}
