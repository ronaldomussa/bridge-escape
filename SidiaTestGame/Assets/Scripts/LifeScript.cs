﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeScript : MonoBehaviour {

    public float objectSpeed = -0.5f;

    void Update()
    {
        transform.Translate(objectSpeed, 0, 0);
    }

}
