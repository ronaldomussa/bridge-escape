﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfeitoRolaBola : MonoBehaviour {

    public float speed = 1;
    public bool rolar = true;
    // Update is called once per frame
    void Update () {
        float offset = Time.time * speed;

        if(rolar)
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, offset);
        else
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offset,0);
    }
}
