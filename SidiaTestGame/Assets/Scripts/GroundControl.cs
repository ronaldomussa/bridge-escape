﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundControl : MonoBehaviour {

    float speed = 10f;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        float offset = Time.time * speed;
        //renderer.material.mainTextureOffset = new Vector2(0, -offset);
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, -offset);
    }
}
