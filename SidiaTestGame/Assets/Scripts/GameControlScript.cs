﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControlScript : MonoBehaviour {

    
    public AudioSource JogoSound;
    public AudioSource PassosSound;
    public AudioSource EndSong;
    public AudioSource GameoverSound;

    public GameObject explosao;

    float timeRemaining = 10;
    float timeExtension = 3f;
    float timeDeduction = 2f;
    float totalTimeElapsed = 0;
    float score = 0f;
    public bool isGameOver = false;
    bool flag = true;

    
    void Start () {
        Time.timeScale = 1;
        JogoSound.Play();
        PassosSound.Play();
        flag = true;
    }


    void Update()
    {
        if (isGameOver) 
            return;

        totalTimeElapsed += Time.deltaTime;
        score = totalTimeElapsed * 100;
        timeRemaining -= Time.deltaTime;

        if (timeRemaining <= 0)
            isGameOver = true;
    }

    void OnGUI()
    {

        if (!isGameOver)
        {
            GUIStyle estiloTitulo = new GUIStyle();
            estiloTitulo.fontSize = 20;
            estiloTitulo.normal.textColor = Color.white;

            GUIStyle estiloLabel = new GUIStyle();
            estiloLabel.fontSize = 50;
            estiloLabel.normal.textColor = Color.white;

            GUI.Box(new Rect(100, 100, 100, 100), "TIME LEFT", estiloTitulo);
            GUI.Box(new Rect(100, 120, 100, 100), ((int)timeRemaining).ToString(), estiloLabel);

            GUI.Box(new Rect(Screen.width - 200, 100, 100, 100), "SCORE", estiloTitulo);
            GUI.Box(new Rect(Screen.width - 200, 120, 100, 100), ((int)score).ToString(), estiloLabel);

            

        }        
        else
        {
            
            if (flag) {
                PassosSound.Stop();
                JogoSound.Stop();
                EndSong.Play();
                GameoverSound.Play();
                flag = false;
            }

            Time.timeScale = 0;

            GUIStyle estiloTitulo = new GUIStyle();
            estiloTitulo.fontSize = 60;
            estiloTitulo.normal.textColor = Color.white;

            GUIStyle estiloLabel = new GUIStyle();
            estiloLabel.fontSize = 45;
            estiloLabel.normal.textColor = Color.white;

            GUIStyle estiloBtn = new GUIStyle();
            estiloBtn.fontSize =40;
            estiloBtn.fontStyle = FontStyle.Bold;
            estiloBtn.normal.textColor = Color.white;


            GUI.Box(new Rect(Screen.width / 3, Screen.height / 5 - 50, Screen.width / 3, 100), "GAME OVER", estiloTitulo);
            GUI.Box(new Rect(Screen.width / 3, Screen.height / 5 + 20, 0, 0), "SCORE: " + ((int)score).ToString(), estiloLabel);

            float py = Screen.height / 4 + Screen.height / 10 + 10;
            float px = Screen.width / 4 + 10;
            float w = Screen.width / 2 - 20;
            float h = Screen.height / 10;

            if (GUI.Button(new Rect(px, py, w , h), "RESTART", estiloBtn))
                SceneManager.LoadScene("Level0");

            if (GUI.Button(new Rect(px, py + 100, w, h), "MAIN MENU", estiloBtn))
                SceneManager.LoadScene("Menu");

            if (GUI.Button(new Rect(px, py + 200, w, h), "EXIT GAME", estiloBtn))
                Application.Quit();

            
        }
    }

    public void VidaColetada()
    {
        timeRemaining += timeExtension;
    }

    public void FogoAtingido()
    {
        
        timeRemaining = 0;
        isGameOver = true;
    }

    public void BolaColidida()
    {
        timeRemaining -= timeDeduction;
    }
}
