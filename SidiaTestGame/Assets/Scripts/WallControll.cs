﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallControll : MonoBehaviour {

    float speed = 2.5f;
    public bool left = true;


    // Update is called once per frame
    void Update()
    {

        float offset = Time.time * speed;

        if (left)
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offset, 0);
        else
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(-offset, 0);
    }
}
