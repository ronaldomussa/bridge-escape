﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HowtoworkScript : MonoBehaviour {

    public GameObject how1;
    public GameObject how2;
    public GameObject how3;
    public GameObject how4;
    int howto = 0;

    void Update()
    {
        Debug.Log("update");
        Debug.Log(howto);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("toque ou clique");
            
            if(howto == 0)
                how2.transform.position = new Vector3(0, -2, howto);
            if (howto == 1)
                how3.transform.position = new Vector3(0, -2, howto);
            if (howto == 2)
                how4.transform.position = new Vector3(0, -2, howto);

            howto++;

            if (howto > 3)
            {

                howto = 0;
                SceneManager.LoadScene("Menu");

                how2.transform.position = new Vector3(0, -140, 0);
                how3.transform.position = new Vector3(0, -140, 0);
                how4.transform.position = new Vector3(0, -140, 0);
            }
        }
    }
}
